local collectors = require 'prometheusClient.collectors.ubus'

local uptime = collectors.uptime()
uptime:read()
print(uptime)

local mem_info = collectors.mem_info()
mem_info:read()
print(mem_info)

local load_avg = collectors.load_avg()
load_avg:read()
print(load_avg)

local iwinfo = collectors.iwinfo()
iwinfo:read()
print(iwinfo)
