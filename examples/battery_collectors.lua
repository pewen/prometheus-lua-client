local collectors = require 'prometheusClient.collectors.proc'

-- Uptime from /proc/uptime
local uptime = collectors.uptime()
uptime:read()
print(uptime)

-- Mem info from /proc/meminfo
-- Optional, you can pass with fields read
local fields = { 'MemFree', 'MemAvailable', 'SwapFree' }
local mem_info = collectors.mem_info(fields)
mem_info:read()
print(mem_info)

-- Load avg from /proc/loadavg
local load_avg = collectors.load_avg()
load_avg:read()
print(load_avg)

-- Packages per interface from /proc/net/dev
local packages_per_interface = collectors.packages_per_interface()
packages_per_interface:read()
print(packages_per_interface)
