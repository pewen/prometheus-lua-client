local client = require 'prometheusClient'

-- Simple Counter
local c1 = client.Counter('http_requests', 'Total number of HTTP request')
c1:inc()   -- By default, increment by 1
c1:inc(27) -- Increment by specific non-negative value
print(c1)

-- Counter with labels
local c2 = client.Counter('http_requests', 'Total number of HTTP request',
  { 'method', 'code' })
c2:inc(6, { 'post', '400' })
c2:inc(1036, { 'post', '200'})
print(c2)

-- Get the last value using the labels names
value, timestamp = c2:value({ 'post', '400' })
-- print(value, timestamp)
-- 6	1548193451

-- Simple Gauge
local g1 = client.Gauge('cpu_tempetature', 'Temperature of the CPU in degrees Celsius')
g1:set(32.6)
g1:inc(0.6)
g1:dec(0.9)
print(g1)

-- Histogram
local h1 = client.Histogram('http_request_duration_seconds', 'A histogram of the request duration.')
h1:observe(2.6)
h1:observe(0.006)
h1:observe(0.004)
print(h1)
