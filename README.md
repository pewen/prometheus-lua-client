# Prometheus Lua client

Prometheus client library for Lua applications. The library is designed object-oriented and respects the best [practices of Prometheus](https://prometheus.io/docs/instrumenting/writing_clientlibs/).

This project is inspired (AKA steal some lines and ideas) from [OpenWRT prometheus-node-exporter-lua](https://github.com/openwrt/packages/tree/master/utils/prometheus-node-exporter-lua) and the official Prometheus Python client.

This project is developed by [AlterMundi](https://altermundi.net/), it is still on development and don't have a release version yet.

![AlterMundi logo](imgs/logo_altermundi.png)
![Logo de libreRouter](imgs/logo_libre_router.jpeg)

## Table of Contents
1. [Install](#install)
2. [Simple examples](#simple-examples)
   1. [Counter](#counter)
   2. [Gauge](#gauge)
   3. [Histogram](#histogram)
   4. [Summary](#summary)
   5. [Instrument](#Instrument)
   6. [Server](#server)
3. [With batteries](#with-batteries)
4. [Roadmap](#roadmap)
5. [Contributions and TODO](#contributions)

## Install

PrometheusLuaClient it just depends on [30log](https://github.com/Yonaba/30log) to make easy de OO programing.

```shell
$ luarocks install PrometheusLuaClient
```

## Simple examples

TODO: make a good doc

### Counter

```lua
local metrics = require 'prometheusClient'

local c1 = metrics.Counter('http_requests', 'Total number of HTTP request')
c1:inc()   -- By default, increment by 1
c1:inc(27) -- Increment by specific non-negative value
print(c1)

-- # HELP http_requests_total Total number of HTTP request
-- # TYPE http_requests_total Counter
-- http_requests_total 28 1548077292
```

Note that the original name does not end in **_total**. The suffix is added according to Prometheus best practices.

You can use labels with the Counter metric

```lua
local c2 = metrics.Counter('http_requests', 'Total number of HTTP request', { 'method', 'code' })
c2:inc(6, { 'post', '400' })
c2:inc(1036, { 'post', '200'})
print(c2)

-- # HELP http_requests_total Total number of HTTP request
-- # TYPE http_requests_total counter
-- http_requests_total{method="post",code="200"} 1036 1548087958
-- http_requests_total{method="post",code="400"} 6 1548087958

```

### Gauge

```lua
local g1 = metrics.Gauge('cpu_temperature', 'Temperature of the CPU in degrees Celsius')
g1:set(32.6)
g1:inc(0.6)
g1:dec(0.9)
print(g1)

-- # HELP cpu_temperature Temperature of the CPU in degrees Celsius
-- # TYPE cpu_temperature gauge
-- cpu_temperature 32.3 1548088212
```

You can get the last value

```lua
value, timestamp = g1.value()
print(value, timestamp)

-- 32.3 1548088212
```

Gauge (and `.value()`) also support labels like Counter.

### Histogram

```lua
local h1 = client.Histogram('http_request_duration_seconds', 'A histogram of the request duration.')
h1:observe(2.6)
h1:observe(0.006)
h1:observe(0.004)
print(h1)

-- # HELP http_request_duration_seconds A histogram of the request duration.
-- # TYPE http_request_duration_seconds histogram
-- http_request_duration_seconds{le="0.005"} 1
-- http_request_duration_seconds{le="0.01"} 2
-- http_request_duration_seconds{le="0.025"} 2
-- http_request_duration_seconds{le="0.05"} 2
-- http_request_duration_seconds{le="0.075"} 2
-- http_request_duration_seconds{le="0.1"} 2
-- http_request_duration_seconds{le="0.25"} 2
-- http_request_duration_seconds{le="0.5"} 2
-- http_request_duration_seconds{le="0.75"} 2
-- http_request_duration_seconds{le="1.0"} 2
-- http_request_duration_seconds{le="2.5"} 2
-- http_request_duration_seconds{le="5.0"} 3
-- http_request_duration_seconds{le="7.5"} 3
-- http_request_duration_seconds{le="10.0"} 3
-- http_request_duration_seconds{le="+Inf"} 3
-- http_request_duration_seconds_sum 2.61
-- http_request_duration_seconds_count 3
```

The Histogram object have a default buckets array, but you can pass your own buckets. You can use the functions `client.generate_buckets()` to generate linear or exponential array of buckets.

### Summary

On progress

### Instrument

On progress

## With batteries!

Implement some basic collectors.

```lua
-- Uptime from /proc/uptime
local uptime = client.collectors.uptime()
uptime:read()
print(uptime)

-- # HELP cpu_uptime CPU uptime on seconds
-- # TYPE cpu_uptime gauge
-- cpu_uptime{type="idle"} 45469.9 1548345842
-- cpu_uptime{type="uptime"} 14160.4 1548345842

-- Meminfo from /proc/meminfo
-- Optional, you can pass with fields read
local fields = { 'MemFree', 'MemAvailable', 'SwapFree' }
local mem_info = client.collectors.mem_info(fields)
mem_info:read()
print(mem_info)

-- # HELP memory_use Use of the memory
-- # TYPE memory_use gauge
-- memory_use{type="MemFree"} 2.51633e+09 1548205909
-- memory_use{type="SwapFree"} 7.99853e+09 1548205909
-- memory_use{type="MemAvailable"} 4.39852e+09 1548205909

-- Load avg from /proc/loadavg
local loadavg = client.collectors.loadavg()
loadavg:read()
print(loadavg)

-- # HELP cpu_load_avg CPU load
-- # TYPE cpu_load_avg gauge
-- cpu_load_avg{type="average_5m"} 1.72 1548345842
-- cpu_load_avg{type="amount_process"} 29544 1548345842
-- cpu_load_avg{type="average_10m"} 1.32 1548345842
-- cpu_load_avg{type="average_1m"} 2.39 1548345842

-- Packages per interface from /proc/net/dev
local packages_per_interface = client.collectors.packages_per_interface()

-- # HELP packages_per_interface Amount of packages on each interface
-- # TYPE packages_per_interface gauge
-- packages_per_interface{interface="602b3742ee4c",mode="receive",type="packets"} 0 1548431468
-- packages_per_interface{interface="lo",mode="transmit",type="drop"} 0 1548431468
-- packages_per_interface{interface="wlxc46e1f1d6364",mode="transmit",type="fifo"} 0 1548431468
-- packages_per_interface{interface="wlxc46e1f1d6364",mode="receive",type="compressed"} 0 1548431468
-- packages_per_interface{interface="wlp3s0",mode="receive",type="frame"} 0 1548431468
-- packages_per_interface{interface="wlp3s0",mode="receive",type="multicast"} 0 1548431468
-- packages_per_interface{interface="docker0",mode="transmit",type="bytes"} 0 1548431468
-- Continue ...
```

`mem_info` use `/proc/meminfo` to get the info. You can pass an array with the name of the properties to use. Be default, read all the properties.

All this collectors are a instance of Instrument. For more details, see [Instrument](#instrument) class doc.

## Roadmap

TODO

## Contributions

This proyect is licence under GPLv3 and try to use [this style guide](https://github.com/Olivine-Labs/lua-style-guide). All contributions are welcome!

### TODO

- [x] LuaRocks packaged
- [ ] The code use [LDoc](https://github.com/stevedonovan/LDoc). Generate a better doc than this readme.
- [ ] Write unitest
- [ ] Implement the function `generate_buckets()`
- [ ] Implement the Summary object
- [ ] Implement some common metric like use of cpu, uptime, temperature sensors.
- [ ] The collector `packages_per_interface` can have a attr to filter some interfaces
