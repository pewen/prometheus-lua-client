package = "PrometheusLuaClient"
version = "0.0.1-1"
source = {
   url = "git://gitlab.com/pewen/prometheus-lua-client",
   tag = "v0.0.1"
}
description = {
   summary = "Prometheus client library writer on Lua.",
   detailed = [[
      Prometheus client library for Lua applications.
      The library is designed object-oriented implementing the 4 types
      of metrics and respects the best practices of Prometheus.
   ]],
   license = "GPLv3",
   homepage = "https://gitlab.com/fnbellomo/prometheus-lua-client",
}
dependencies = {
   "lua >= 5.1, <= 5.4",
   "30log >= 1.3.0"
}
build = {
  type = "builtin",
  modules = {
      ["prometheusClient"] = "src/core.lua",
      ["prometheusClient.metrics"] = "src/metrics.lua",
      ["prometheusClient.instrument"] = "src/instrument.lua",
      ["prometheusClient.collectors"] = "src/collectors/core.lua",
      ["prometheusClient.collectors.uptime"] = "src/collectors/uptime.lua",
      ["prometheusClient.collectors.loadavg"] = "src/collectors/loadavg.lua",
      ["prometheusClient.collectors.mem_info"] = "src/collectors/mem_info.lua",
      ["prometheusClient.collectors.packages_per_interface"] = "src/collectors/packages_per_interface.lua"
  }
}
